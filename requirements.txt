asgiref==3.2.3
Django==3.0.1
gunicorn==20.0.4
importlib-metadata==1.6.0
jedi==0.15.2
parso==0.7.0
pluggy==0.13.1
pytz==2019.3
sqlparse==0.3.0
virtualenv==16.4.0
whitenoise==5.0.1
zipp==3.1.0
psycopg2-binary==2.8.5
dj-database-url
python-dateutil==2.8.1

